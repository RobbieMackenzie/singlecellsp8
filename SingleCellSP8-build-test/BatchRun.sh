#!/bin/bash

# This bash script runs the GEANT4 simulation several times, using sed to
# change the value of the photon energy in the macro file.

function Run {
    local label=$2
    local threads=$1
    echo " "
    echo $label
    echo " "

    # Change the gamma energy in the macro file
    sed -i "20 s/\/gps\/pos\/centre .*/\/gps\/pos\/centre $label 9 mm/" SingleCellSP8.mac

    # Run the model with the macro file, dumping output for speed
    ./radioprotection SingleCellSP8.mac > /dev/null

    # Process the data into raw and summary files
    python3 DataProcess.py -j$threads -l"$label" -f"./" >> CollectedData.csv
}
################################################################################

threads=6
echo "###################### Batch Run Starting ########################"
for n in -2 -1.5 -1 -0.5 0 0.5 1 1.5 2 2.5 3
    do
    Run $threads "0 $n"
    done
for n in -1.5 -1 -0.5 0 0.5 1 1.5 2 2.5 3 3.5 4
    do
    Run $threads "$n 0"
    done

